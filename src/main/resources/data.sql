INSERT INTO adresa (id, ulica, broj) VALUES (1,'Bulevar Cara Lazara', 5);
INSERT INTO adresa (id, ulica, broj) VALUES (2, 'Dalmatinska', 7);

INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga, adresa_id)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN',1);
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga, adresa_id)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK',2);
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga, adresa_id)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK',2);

INSERT INTO sprint (id, ime, ukupno_bodova) VALUES (1, 'Sprint1', 13);
INSERT INTO sprint (id, ime, ukupno_bodova) VALUES (2, 'Sprint2', 16);
INSERT INTO sprint (id, ime, ukupno_bodova) VALUES (3, 'Sprint3', 6);
INSERT INTO sprint (id, ime, ukupno_bodova) VALUES (4, 'Sprint4', 0);

INSERT INTO stanje (id, stanje) VALUES (1, 'NOV');
INSERT INTO stanje (id, stanje) VALUES (2, 'U_TOKU');
INSERT INTO stanje (id, stanje) VALUES (3, 'ZAVRSEN');


INSERT INTO zadatak (id, ime, zaduzeni, bodovi, sprint_id, stanje_id) VALUES (1, 'Kreirati REST servis', 'Nikola', 8, 1, 1);
INSERT INTO zadatak (id, ime, zaduzeni, bodovi, sprint_id, stanje_id) VALUES (2, 'Kreirati pocetnu stranicu', 'Bane', 5, 1, 2);
INSERT INTO zadatak (id, ime, zaduzeni, bodovi, sprint_id, stanje_id) VALUES (3, 'Kreirati LOGO', 'Ana', 8, 2, 3);
INSERT INTO zadatak (id, ime, zaduzeni, bodovi, sprint_id, stanje_id) VALUES (4, 'Izmeniti buttons', 'Ana', 8, 2, 1);
INSERT INTO zadatak (id, ime, zaduzeni, bodovi, sprint_id, stanje_id) VALUES (5, 'Dodati opciju za password change', 'Bane', 6, 3, 1);