package com.matkovic.jwd48.sprint.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.matkovic.jwd48.sprint.model.Adresa;


@Repository
public interface AdresaRepository extends JpaRepository<Adresa, Long> {

}
